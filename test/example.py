#!/usr/bin/python
# -*- mode:python; coding:utf-8; tab-width:4 -*-

import sys
import logging
from loggingx import (JabberHandler, SMTP_SSLHandler, SimpleSMTP_SSLHandler,
                      Server, Account)

sender = Account(username='your-account@gmail.com',
                 password='your-password')

destination = 'target-account@gmail.com'

jabber_handler = JabberHandler(sender=sender, to=destination)

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
logger.addHandler(jabber_handler)

logger.debug("debug message")


# SMTP
smtp_handler = SMTP_SSLHandler(mailhost    = ('smtp.gmail.com', 587),
                               fromaddr    = sender.username,
                               credentials = sender.credentials,
                               toaddrs     = [destination],
                               subject     = 'SSL SMTP notification',
                               ssl         = True)

# or
gmail_smtp = Server(hostname='smtp.gmail.com', port=587, ssl=True)

smtp_handler = SimpleSMTP_SSLHandler(server  = gmail_smtp,
                                     sender  = sender,
                                     toaddrs = [destination],
                                     subject = 'SSL SMTP notification')

logger.addHandler(smtp_handler)
logger.debug("debug message")
